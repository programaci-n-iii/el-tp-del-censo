# Trabajo práctico del censo

Trabajo práctico universitario en el que se plantea optimizar el número de censistas necesarios para llevar a cabo un censo en una ciudad. Dicho problema se resolverá con la aplicación de teoría de grafos, algoritmos y heurísticas. 

Se utilizará Java complementando con Swing para la interfaz visual.
