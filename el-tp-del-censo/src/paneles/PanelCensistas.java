package paneles;

import java.awt.Cursor;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import algoritmos.SolverGoloso;
import main.Censista;
import main.RadioCensal;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import java.awt.Color;

@SuppressWarnings("serial")
public class PanelCensistas extends JPanel {

	private int _indice;
	private List<Censista> _censistas;

	public PanelCensistas(JFrame frame, RadioCensal radio, JMapViewer mapa) {
		setSize(800, 600);
		setLayout(null);

		int cantCensistasAsignados = obtenerCantCensistasAsignados(radio);
		_censistas = new ArrayList<Censista>(radio.getCensistasConManzanas());

		mapa.setBounds(25, 90, 450, 435);
		add(mapa);

		JLabel textoCantCensistasAsignados = new JLabel("Para " + radio.tamano()
				+ " manzanas se necesita " + cantCensistasAsignados + " censista/s.");
		textoCantCensistasAsignados.setFont(new Font("Dialog", Font.PLAIN, 16));
		textoCantCensistasAsignados.setBounds(241, 52, 485, 27);
		add(textoCantCensistasAsignados);

		JLabel fotoCensista = new JLabel(new ImageIcon(_censistas.get(_indice).getFoto()));
		fotoCensista.setFont(new Font("Dialog", Font.PLAIN, 15));
		fotoCensista.setBounds(496, 116, 260, 260);
		add(fotoCensista);
		
		JTextArea datosCensista = new JTextArea();
		datosCensista.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		datosCensista.setFont(new Font("Dialog", Font.PLAIN, 15));
		datosCensista.setEditable(false);
		datosCensista.setBounds(496, 387, 260, 82);
		actualizarDatos(datosCensista, fotoCensista);
		add(datosCensista);

		JButton botonSiguiente = new JButton("Siguiente");
		botonSiguiente.addActionListener(e -> {
			cambiarDeCensista(i -> {return ++i;}, i -> {return i >= _censistas.size();}, i -> {return i;});
			actualizarDatos(datosCensista, fotoCensista);
		});
		botonSiguiente.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonSiguiente.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonSiguiente.setBounds(637, 480, 119, 29);
		add(botonSiguiente);

		JButton botonAnterior = new JButton("Anterior");
		botonAnterior.addActionListener(e -> {
			cambiarDeCensista(i -> {return --i;}, i -> {return i < 0;}, i -> {return _censistas.size() - 1;});
			actualizarDatos(datosCensista, fotoCensista);
		});
		botonAnterior.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonAnterior.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonAnterior.setBounds(496, 480, 119, 29);
		add(botonAnterior);

		JLabel imagenFondo = new JLabel(new ImageIcon("images/fondo.png"));
		imagenFondo.setBounds(0, 0, 800, 600);
		add(imagenFondo);
	}
	
	private void actualizarDatos(JTextArea datos, JLabel foto) {
		datos.setText("DNI: " + _censistas.get(_indice).getDni() +
				"\nNombre: " + _censistas.get(_indice).getNombre() +
				"\nCantidad de manzanas asignadas: " + _censistas.get(_indice).cantidadManzanas() + 
				"\nManzanas asignadas: " + _censistas.get(_indice).getManzanas());
		foto.setIcon(new ImageIcon(_censistas.get(_indice).getFoto()));
	}

	private int obtenerCantCensistasAsignados(RadioCensal radioElegido) {
		SolverGoloso algoritmo = new SolverGoloso(radioElegido);
		algoritmo.resolver();
		return algoritmo.getCantCensistasAsignados();
	}

	private void cambiarDeCensista(Function<Integer, Integer> f1, Predicate<Integer> pred, Function<Integer, Integer> f2) {
		_indice = f1.apply(_indice);
		if (pred.test(_indice)) {
			_indice = f2.apply(0);
		}
	}
}
