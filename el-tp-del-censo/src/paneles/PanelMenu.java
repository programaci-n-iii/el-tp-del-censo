package paneles;

import javax.swing.JPanel;
import main.MainWindow;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Cursor;

@SuppressWarnings("serial")
public class PanelMenu extends JPanel {

	public PanelMenu(JFrame frame) {
		setSize(800, 600);
		setLayout(null);
		
		JButton botonIniciar = new JButton("Asignar manzanas");
		botonIniciar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonIniciar.addActionListener(e -> MainWindow.pasarAlOtroPanel(frame, PanelMenu.this, new PanelArchivos(frame, "paneles.PanelCensistas")));
		botonIniciar.setFont(new Font("Dialog", Font.PLAIN, 17));
		botonIniciar.setBounds(268, 94, 214, 35);
		add(botonIniciar);
		
		JButton botonCompararTiempos = new JButton("Comparar algoritmos");
		botonCompararTiempos.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonCompararTiempos.addActionListener(e -> MainWindow.pasarAlOtroPanel(frame, PanelMenu.this, new PanelArchivos(frame, "paneles.PanelComparacion")));
		botonCompararTiempos.setFont(new Font("Dialog", Font.PLAIN, 17));
		botonCompararTiempos.setBounds(268, 140, 214, 35);
		add(botonCompararTiempos);
		
		JButton botonSalir = new JButton("Salir del programa");
		botonSalir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonSalir.setFont(new Font("Dialog", Font.PLAIN, 17));
		botonSalir.addActionListener(e -> frame.dispose());
		botonSalir.setBounds(268, 186, 214, 35);
		add(botonSalir);
		
		JLabel imagenFondo = new JLabel(new ImageIcon("images/fondo.png"));
		imagenFondo.setBounds(0, 0, 800, 600);
		add(imagenFondo);
	}
}
