package paneles;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import main.RadioCensal;
import javax.swing.JLabel;
import java.awt.Cursor;
import java.awt.Font;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import org.openstreetmap.gui.jmapviewer.JMapViewer;
import auxiliares.TiemposDeEjecucion;

@SuppressWarnings("serial")
public class PanelComparacion extends JPanel {
	
	private TiemposDeEjecucion _tiempos;

	public PanelComparacion(JFrame frame, RadioCensal radio, JMapViewer mapa) {
		setSize(800, 600);
		setLayout(null);
		
		mapa.setBounds(290, 65, 460, 435);
		add(mapa);
		
		_tiempos = new TiemposDeEjecucion(radio);
		
		JLabel textoSolverGoloso = new JLabel("Solver Goloso");
		textoSolverGoloso.setFont(new Font("Dialog", Font.BOLD, 20));
		textoSolverGoloso.setBounds(50, 101, 180, 30);
		add(textoSolverGoloso);
		
		JTextArea datosCensistasGoloso = new JTextArea();
		datosCensistasGoloso.setEditable(false);
		datosCensistasGoloso.setFont(new Font("Dialog", Font.PLAIN, 15));
		datosCensistasGoloso.setText("Cantidad de censistas: 0\n\nTiempo de ejecución: ");
		datosCensistasGoloso.setBounds(50, 179, 220, 80);
		add(datosCensistasGoloso);
		
		JScrollPane scrollDatosGoloso = new JScrollPane(datosCensistasGoloso, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollDatosGoloso.setBounds(50, 179, 220, 80);
		add(scrollDatosGoloso);
		
		JButton botonVerGoloso = new JButton("Ver");
		botonVerGoloso.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonVerGoloso.addActionListener(e -> botonVerGoloso.setEnabled(false));
		botonVerGoloso.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonVerGoloso.addActionListener(e -> obtenerDatosAlgoritmo(datosCensistasGoloso, t -> t.tomarTiempoSG(), t -> t.datosSG()));
		botonVerGoloso.setBounds(50, 140, 66, 27);
		add(botonVerGoloso);
		
		JLabel textoSolverFuerzaBruta = new JLabel("Solver Fuerza Bruta");
		textoSolverFuerzaBruta.setFont(new Font("Dialog", Font.BOLD, 20));
		textoSolverFuerzaBruta.setBounds(50, 300, 220, 30);
		add(textoSolverFuerzaBruta);
		
		JTextArea datosCensistasFB = new JTextArea();
		datosCensistasFB.setEditable(false);
		datosCensistasFB.setFont(new Font("Dialog", Font.PLAIN, 15));
		datosCensistasFB.setText("Cantidad de censistas: 0\n\nTiempo de ejecución: ");
		datosCensistasFB.setBounds(50, 380, 220, 80);
		add(datosCensistasFB);
		
		JScrollPane scrollDatosFB = new JScrollPane(datosCensistasFB, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollDatosFB.setBounds(50, 380, 220, 80);
		add(scrollDatosFB);
		
		JButton botonVerFB = new JButton("Ver");
		botonVerFB.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonVerFB.addActionListener(e -> botonVerFB.setEnabled(false));
		botonVerFB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonVerFB.addActionListener(e -> obtenerDatosAlgoritmo(datosCensistasFB, t -> t.tomarTiempoSFB(), t -> t.datosSFB()));
		botonVerFB.setBounds(50, 340, 66, 27);
		add(botonVerFB);
		
		JButton botonSalir = new JButton("Salir del programa");
		botonSalir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonSalir.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonSalir.addActionListener(e -> frame.dispose());
		botonSalir.setBounds(572, 511, 180, 30);
		add(botonSalir);
		
		JLabel imagenFondo = new JLabel(new ImageIcon("images/fondo.png"));
		imagenFondo.setBounds(0, 0, 800, 600);
		add(imagenFondo);
	}
	
	private void obtenerDatosAlgoritmo(JTextArea datos, Consumer<TiemposDeEjecucion> c, Function<TiemposDeEjecucion, String> f) {
		datos.setText(datos.getText() + "Cargando...");
		new Thread() {
			
			public void run() {
				c.accept(_tiempos);
				datos.setText(f.apply(_tiempos));
			}
			
		}.start();
	}
}

