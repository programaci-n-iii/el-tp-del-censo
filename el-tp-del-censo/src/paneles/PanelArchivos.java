package paneles;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;

import auxiliares.ManejoDeArchivos;
import main.MainWindow;
import main.RadioCensal;

@SuppressWarnings("serial")
public class PanelArchivos extends JPanel {
	
	private JMapViewer _mapa;
	private RadioCensal _radioElegido;

	public PanelArchivos(JFrame frame, String siguientePanel) {
		setSize(800, 600);
		setLayout(null);

		Coordinate coordenadaUNGS = new Coordinate(-34.521, -58.7008);

		_mapa = new JMapViewer();
		_mapa.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		_mapa.setBounds(23, 163, 738, 382);
		_mapa.setZoomControlsVisible(false);
		_mapa.setDisplayPosition(coordenadaUNGS, 16);
		add(_mapa);

		JComboBox<String> seleccionArchivos = new JComboBox<>();
		seleccionArchivos.addActionListener(e -> actualizarMapa(seleccionArchivos));
		seleccionArchivos.setFont(new Font("Dialog", Font.PLAIN, 15));
		seleccionArchivos.setBounds(168, 90, 375, 34);
		seleccionArchivos.setModel(new DefaultComboBoxModel<>(new String[] {}));
		List<String> jsons = ManejoDeArchivos.RADIOS_JSON;
		for (String json : jsons) {
			seleccionArchivos.addItem(json);
		}
		add(seleccionArchivos);

		actualizarMapa(seleccionArchivos);

		JLabel textoElegirArchivo = new JLabel("Seleccione el archivo que desee:");
		textoElegirArchivo.setFont(new Font("Dialog", Font.PLAIN, 17));
		textoElegirArchivo.setBounds(251, 50, 270, 29);
		add(textoElegirArchivo);

		JLabel textoIndicarMovimientoMapa = new JLabel("(Con click derecho puede mover el mapa a su preferencia)");
		textoIndicarMovimientoMapa.setFont(new Font("Dialog", Font.PLAIN, 15));
		textoIndicarMovimientoMapa.setBounds(178, 135, 393, 17);
		add(textoIndicarMovimientoMapa);
		
		JButton botonSeleccionar = new JButton("Seleccionar");
		botonSeleccionar.addActionListener(e -> pasarAlOtroPanel(frame, siguientePanel));
		botonSeleccionar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonSeleccionar.setFont(new Font("Dialog", Font.PLAIN, 17));
		botonSeleccionar.setBounds(553, 90, 127, 34);
		add(botonSeleccionar);

		JLabel imagenFondo = new JLabel(new ImageIcon("images/fondo.png"));
		imagenFondo.setBounds(0, 0, 800, 600);
		add(imagenFondo);
	}

	private void actualizarMapa(JComboBox<String> seleccionArchivos) {
		guardarRadioElegido(seleccionArchivos);
		actualizarMarcadores();
	}

	private void guardarRadioElegido(JComboBox<String> seleccionArchivos) {
		String jsonSeleccionado = (String) seleccionArchivos.getSelectedItem();
		_radioElegido = ManejoDeArchivos.leerJSONdeRadioCensal(jsonSeleccionado);
	}

	private void actualizarMarcadores() {
		_mapa.removeAllMapMarkers();
		Map<Integer, List<Double>> coordenadasManzanas = _radioElegido.getCoordenadasManzanas();
		for (int i = 0; i < coordenadasManzanas.size(); i++) {
			double x = coordenadasManzanas.get(i).get(0);
			double y = coordenadasManzanas.get(i).get(1);
			_mapa.addMapMarker(new MapMarkerDot("Manzana " + i, new Coordinate(x, y)));
		}
	}
	
	private void pasarAlOtroPanel(JFrame frame, String siguientePanel) {
		if(siguientePanel.equals(PanelCensistas.class.getName())) {
			MainWindow.pasarAlOtroPanel(frame, PanelArchivos.this, new PanelCensistas(frame, _radioElegido, _mapa));
		} else if(siguientePanel.equals(PanelComparacion.class.getName())) {
			MainWindow.pasarAlOtroPanel(frame, PanelArchivos.this, new PanelComparacion(frame, _radioElegido, _mapa));
		}
	}
}
