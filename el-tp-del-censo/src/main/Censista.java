package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Censista implements Comparable<Censista> {
	
	private int _dni;
	private String _nombre;
	private String _foto;
	private List<Integer> _manzanasAsignadas;
	
	public Censista(int dni, String nombre) {
		inicializarCensista(dni, nombre);
	}
	
	public Censista(int dni, String nombre, String foto) {
		inicializarCensista(dni, nombre);
		_foto = foto;
	}

	public Censista(Censista censista) {
		_dni = censista._dni;
		_nombre = censista._nombre;
		_foto = censista._foto;
		_manzanasAsignadas = censista._manzanasAsignadas;
	}

	private void inicializarCensista(int dni, String nombre) {
		_dni = dni;
		_nombre = nombre;
		_manzanasAsignadas = new ArrayList<>();
	}
	
	public void asignar(int manzana) {
		if(!_manzanasAsignadas.contains(manzana)) {
			_manzanasAsignadas.add(manzana);
		}
	}
	
	public int cantidadManzanas() {
		return _manzanasAsignadas.size();
	}
	
	public boolean tieneManzanas() {
		return !_manzanasAsignadas.isEmpty();
	}
	
	public int getManzana(int i) {
		return _manzanasAsignadas.get(i);
	}
	
	public List<Integer> getManzanas() {
		return new ArrayList<>(_manzanasAsignadas);
	}
	
	public static Set<Censista> clonarCensistasSinManzanas(Set<Censista> censistas) {
		Set<Censista> ret = new TreeSet<>();
		for (Censista censista : censistas) {
			ret.add(new Censista(censista.getDni(), censista.getNombre(), censista.getFoto()));
		}
		return ret;
	}

	@Override
	public int compareTo(Censista censista) {
		if(_dni == censista._dni) {
			return 0;
		} else if(_dni > censista._dni) {
			return -1;
		}
		return 1;
	}

	@Override
	public String toString() {
		StringBuilder st = new StringBuilder();
		st.append(_dni);
		st.append(" (");
		st.append(_nombre);
		st.append(") tiene las manzanas: ");
		st.append(_manzanasAsignadas);
		return st.toString();
	}
	
	public int getDni() {
		return _dni;
	}
	
	public String getNombre() {
		return _nombre;
	}

	public String getFoto() {
		return _foto;
	}
}
