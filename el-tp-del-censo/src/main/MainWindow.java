package main;

import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import com.formdev.flatlaf.intellijthemes.FlatGrayIJTheme;
import paneles.PanelMenu;

public class MainWindow {

	private JFrame _frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window._frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainWindow() {
		FlatGrayIJTheme.setup();
		initialize();
	}

	private void initialize() {
		_frame = new JFrame();
		_frame.setTitle("Censo");
		_frame.setSize(800, 600);
		_frame.setResizable(false);
		_frame.setLocationRelativeTo(null);
		_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel titulo = new JLabel("Censo 2022");
		titulo.setFont(new Font("Dialog", Font.BOLD, 25));
		titulo.setBounds(302, 11, 150, 25);
		_frame.getContentPane().add(titulo);
		
		PanelMenu panelMenu = new PanelMenu(_frame);
		_frame.getContentPane().add(panelMenu);
	}
	
	public static void pasarAlOtroPanel(JFrame frame, JPanel panelActual, JPanel otro) {
		panelActual.setVisible(false);
		frame.getContentPane().add(otro);
	}
}
