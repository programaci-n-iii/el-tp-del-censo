package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import algoritmos.Grafo;

public class RadioCensal {
	
	private Grafo _grafo;
	private Set<Censista> _censistas;
	private Map<Integer, List<Double>> _coordenadasManzanas;
	
	public RadioCensal(int manzanas) {
		_grafo = new Grafo(manzanas);
		_censistas = new TreeSet<>();
		inicializarCoordenadasManzanas();
	}
	
	public RadioCensal(RadioCensal r) {
		_grafo = r.getGrafo();
		_censistas = r.getCensistas();
		_coordenadasManzanas = r.getCoordenadasManzanas();
	}
	
	public void agregarCensista(int dni, String nombre) {
		_censistas.add(new Censista(dni, nombre));
	}
	
	public void agregarCensista(int dni, String nombre, String foto) {
		_censistas.add(new Censista(dni, nombre, foto));
	}
	
	public void setManzanasVecinas(int i, int j) {
		_grafo.agregarArista(i, j);
	}
	
	public boolean sonManzanasVecinas(int i, int j) {
		return _grafo.existeArista(i, j);
	}

	public int tamano() {
		return _grafo.tamano();
	}
	
	public void asignarCoordenadaAManzana(int manzana, double x, double y) {
		verificarManzana(manzana);
		_coordenadasManzanas.replace(manzana, List.of(x, y));
	}
	
	private void verificarManzana(int manzana) {
		if(manzana < 0) {
			throw new IllegalArgumentException("Manzana invalida: el numero de la manzana debe ser mayor a 0");
		} else if(manzana >= _grafo.tamano()) {
			throw new IllegalArgumentException("Manzana invalida: el numero de la manzana debe ser menor a " + _grafo.tamano());
		}
	}
	
	public Set<Censista> getCensistas() {
		return new TreeSet<>(_censistas);
	}
	
	public Set<Censista> getCensistasConManzanas() {
		Set<Censista> ret = new TreeSet<>();
		for(Censista censista: _censistas) {
			if(!censista.getManzanas().isEmpty()) {
				ret.add(censista);
			}
		}
		return ret;
	}

	public void setCensistas(Set<Censista> censistas) {
		_censistas = censistas;
	}

	public Map<Integer, List<Double>> getCoordenadasManzanas() {
		return new TreeMap<Integer, List<Double>>(_coordenadasManzanas);
	}
	
	public Grafo getGrafo() {
		return new Grafo(_grafo);
	}
	
	private void inicializarCoordenadasManzanas() {
		_coordenadasManzanas = new TreeMap<>();
		for (int i = 0; i < _grafo.tamano(); i++) {
			_coordenadasManzanas.put(i, new ArrayList<>(2));
		}
	}

}
