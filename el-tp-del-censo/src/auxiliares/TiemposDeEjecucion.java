package auxiliares;

import algoritmos.SolverFuerzaBruta;
import algoritmos.SolverGoloso;
import main.RadioCensal;

public class TiemposDeEjecucion {

	private RadioCensal _radio;
	private SolverGoloso _solverG;
	private SolverFuerzaBruta _solverFB;
	private Float _tiempoSG;
	private Float _tiempoSFB;

	public TiemposDeEjecucion(RadioCensal radio) {
		_radio = radio;
	}

	public void tomarTiempoSG() {
		long tiempoInicio, tiempoFinal;
		tiempoInicio = System.currentTimeMillis();
		
		_solverG = new SolverGoloso(_radio);
		_solverG.resolver();
		
		tiempoFinal = System.currentTimeMillis();
		_tiempoSG = (float) (tiempoFinal - tiempoInicio);
	}

	public void tomarTiempoSFB() {
		long tiempoInicio, tiempoFinal;
		tiempoInicio = System.currentTimeMillis();
		
		_solverFB = new SolverFuerzaBruta(_radio);
		
		tiempoFinal = System.currentTimeMillis();
		_tiempoSFB = (float) (tiempoFinal - tiempoInicio);
	}
		
	
	public String datosSG() {
		return datos(_solverG.getCantCensistasAsignados(), _tiempoSG);
	}
	
	public String datosSFB() {
		return datos(_solverFB.solucion().size(), _tiempoSFB);
	}
	
	private String datos(int censistas, float tiempo) {
		return "Cantidad de censistas: " + censistas + "\n\n" +
				"Tiempo de ejecución: " + tiempo / 1000.0 + " segundos (" + tiempo + " milisegundos).";
	}
}
