package auxiliares;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import com.google.gson.Gson;

import main.RadioCensal;

public class ManejoDeArchivos {

	public final static List<String> RADIOS_JSON = List.of("files/Radio con 4 manzanas contiguas",
			"files/Radio con 6 manzanas", "files/Radio con 6 manzanas no contiguas",
			"files/Radio con 3 manzanas contiguas", "files/Radio con 5 manzanas", "files/Radio con 10 manzanas",
			"files/Radio con 12 manzanas contiguas", "files/Radio con 14 manzanas",
			"files/Radio con 14 manzanas no contiguas");

	public static RadioCensal leerJSONdeRadioCensal(String archivo) {
		Gson gson = new Gson();
		RadioCensal ret = null;

		try {
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, RadioCensal.class);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return ret;
	}
}
