package tests;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;


import org.junit.Before;
import org.junit.Test;

import algoritmos.SolverFuerzaBruta;
import main.Censista;
import main.RadioCensal;

public class SolverFuerzaBrutaTest {
	
	private RadioCensal _radioTamanoSeis, _radioTamanoTres, _radioCliqueCinco, 
	_radioCinco, _radioTamanoCuatro;

	@Before
	public void setUp() {		
		_radioTamanoSeis = new RadioCensal(6); 
		_radioTamanoTres = new RadioCensal(3);
		_radioCliqueCinco = new RadioCensal(5);
		_radioCinco = new RadioCensal(5);
		_radioTamanoCuatro = new RadioCensal(4);
		
		setVecindadRadioClique();
	}
	
	@Test
	public void radioCincoTest() {
		_radioCinco.setManzanasVecinas(0, 1);
		_radioCinco.setManzanasVecinas(1, 2);
		_radioCinco.setManzanasVecinas(2, 3);
		_radioCinco.setManzanasVecinas(2, 4);
		
		agregarCensistas(_radioCinco);
		
		SolverFuerzaBruta s = new SolverFuerzaBruta(_radioCinco);
		assertEquals(2, s.solucion().size());
		
	}
	
	@Test
	public void cantManzanasMaxPorCesistaTest() {
		_radioTamanoCuatro.setManzanasVecinas(0, 1);
		_radioTamanoCuatro.setManzanasVecinas(1, 2);
		_radioTamanoCuatro.setManzanasVecinas(2, 3);
		
		agregarCensistas(_radioTamanoCuatro);
		
		SolverFuerzaBruta s = new SolverFuerzaBruta(_radioTamanoCuatro);
		assertEquals(2, s.solucion().size());
	}
	
	@Test
	public void manzanasContiguasTest() {
		_radioTamanoSeis.setManzanasVecinas(0, 1);
		_radioTamanoSeis.setManzanasVecinas(1, 2);
		_radioTamanoSeis.setManzanasVecinas(2, 3);
		_radioTamanoSeis.setManzanasVecinas(3, 4);
		_radioTamanoSeis.setManzanasVecinas(4, 5);
		SolverFuerzaBruta s = new SolverFuerzaBruta(_radioTamanoSeis);
		assertEquals(2, s.getMejorRecorrido().censistasNecesarios());
	}
	
	@Test
	public void vecindadTresAtres() {
		_radioTamanoSeis.setManzanasVecinas(0, 1);
		_radioTamanoSeis.setManzanasVecinas(1, 2);
		_radioTamanoSeis.setManzanasVecinas(3, 4);
		_radioTamanoSeis.setManzanasVecinas(4, 5);
		SolverFuerzaBruta s = new SolverFuerzaBruta(_radioTamanoSeis);
		assertEquals(2, s.getMejorRecorrido().censistasNecesarios());
	}
	
	@Test
	public void vecindadDosAdos() {
		_radioTamanoSeis.setManzanasVecinas(0, 1);
		_radioTamanoSeis.setManzanasVecinas(2, 3);
		_radioTamanoSeis.setManzanasVecinas(4, 5);
		SolverFuerzaBruta s = new SolverFuerzaBruta(_radioTamanoSeis);
		assertEquals(3, s.getMejorRecorrido().censistasNecesarios());
	}
	
	@Test
	public void manzanasAisladas() {
		SolverFuerzaBruta s = new SolverFuerzaBruta(_radioTamanoTres);
		assertEquals(3, s.getMejorRecorrido().censistasNecesarios());
	}
	
	@Test
	public void radioCliqueTamanoCinco() {
		SolverFuerzaBruta s = new SolverFuerzaBruta(_radioCliqueCinco);
		assertEquals(2, s.getMejorRecorrido().censistasNecesarios());
	}
	
	@Test
	public void asignacionCensistasHappyPath() {
		agregarCensistas(_radioCliqueCinco);
		SolverFuerzaBruta s = new SolverFuerzaBruta(_radioCliqueCinco);
		assertEquals(2, s.solucion().size());
	}
	
	@Test
	public void asignacionCensistasRadioAislado() {
		agregarCensistas(_radioTamanoTres);
		SolverFuerzaBruta s = new SolverFuerzaBruta(_radioTamanoTres);
		assertEquals(3, s.solucion().size());
	}
	
	@Test
	public void asignacionDonUnicosCensistas() {
		_radioCliqueCinco.agregarCensista(40758663, "A");
		_radioCliqueCinco.agregarCensista(38752684, "B");
		SolverFuerzaBruta s = new SolverFuerzaBruta(_radioCliqueCinco);
		
		Censista a = new Censista(40758663, "A");
		Censista b = new Censista(38752684, "B");
		
		Set<Censista> censistas = new HashSet<>(); 
		censistas.add(a);
		censistas.add(b);
		
		assertTrue(isEqualsCensistas(censistas, s.solucion()));
	}
	
	private boolean isEqualsCensistas(Set<Censista> censistas, Set<Censista> solucion) {
		boolean ret = true;
		boolean check = false;
		
		for (Censista s : solucion) {
			for (Censista c : censistas) {
				check = check || c.compareTo(s) == 0;
			}
			ret = ret && check;
		}		
		return ret;
	}
	
	private void setVecindadRadioClique() {
		for (int i = 0; i < 5; i++) {
			for (int j = 1; j < 5; j++) {
				if (j != i) {
					_radioCliqueCinco.setManzanasVecinas(i, j);
				}
			}
		}
	}
	
	private void agregarCensistas(RadioCensal radio) {
		radio.agregarCensista(40758663, "A");
		radio.agregarCensista(38752684, "B");
		radio.agregarCensista(36283728, "C");
		radio.agregarCensista(39857263, "D");
		radio.agregarCensista(35472093, "E");
	}
}
