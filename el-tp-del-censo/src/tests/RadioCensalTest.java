package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;
import main.Censista;
import main.RadioCensal;

public class RadioCensalTest {
	
	private RadioCensal _radio;
	
	@Before
	public void setUp() {
		_radio = new RadioCensal(4);
	}

	@Test
	public void agregarCensistaTest() {
		_radio.agregarCensista(40758663, "A");
		_radio.agregarCensista(38752684, "B");
		_radio.agregarCensista(36283728, "C");
		
		Set<Censista> esperados = new TreeSet<>();
		esperados.add(new Censista(40758663, "A"));
		esperados.add(new Censista(38752684, "B"));
		esperados.add(new Censista(38752684, "C"));
		
		censistasEquals(esperados, _radio.getCensistas());
	}

	private void censistasEquals(Set<Censista> censistas, Set<Censista> censistas2) {
		boolean ret = true;
		for(Censista censista : censistas) {
			ret = ret && censistas2.contains(censista);
		}
		assertTrue(ret);
	}

	@Test
	public void asignarCoordenadaTest() {
		_radio.asignarCoordenadaAManzana(0, -34.52126711205503, -58.70413541793823);
		
		List<Double> esperado = List.of(-34.52126711205503, -58.70413541793823);
		assertEquals(esperado, _radio.getCoordenadasManzanas().get(0));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void asignarCoordenadaConManzanaNegativaTest() {
		_radio.asignarCoordenadaAManzana(-1, -34.52126711205503, -58.70413541793823);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void asignarCoordenadaConManzanaMayorAlTamanoTest() {
		_radio.asignarCoordenadaAManzana(5, -34.52126711205503, -58.70413541793823);
	}

}
