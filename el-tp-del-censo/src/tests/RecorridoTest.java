package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import algoritmos.Recorrido;

import main.RadioCensal;

public class RecorridoTest {

	RadioCensal _radio1, _radio2, _radio3;
	Recorrido _recorrido, _recorrido2, _recorrido3;

	@Before
	public void setUp() throws Exception {
		_radio1 = new RadioCensal(4);
		_radio2 = new RadioCensal(8);
		_radio3 = new RadioCensal(5);
		
		_radio1.setManzanasVecinas(0, 1);
		_radio1.setManzanasVecinas(1, 2);
		_radio1.setManzanasVecinas(2, 3);
		
		_radio3.setManzanasVecinas(0, 1);
		_radio3.setManzanasVecinas(1, 2);
		_radio3.setManzanasVecinas(2, 3);
		_radio3.setManzanasVecinas(2, 4);

		// recorrido [0, 1, 2, 3]
		_recorrido = new Recorrido(_radio1);
//		// ciudad disconexa
		_recorrido2 = new Recorrido(_radio2);
		// ciuadad de cinco manzanas
		_recorrido3 = new Recorrido(_radio3);
	}

	@Test
	public void testCensistasNecesarios() {
		_recorrido.agregar(0);
		_recorrido.agregar(1);
		_recorrido.agregar(2);
		_recorrido.agregar(3);
		
		assertEquals(2, _recorrido.censistasNecesarios());
	}

	@Test
	public void testCasoSinManzanasVecinas() {
		_recorrido2.agregar(0);
		_recorrido2.agregar(1);
		_recorrido2.agregar(2);
		_recorrido2.agregar(3);
		_recorrido2.agregar(4);
		_recorrido2.agregar(5);
		_recorrido2.agregar(6);
		_recorrido2.agregar(7);
		
		assertEquals(8, _recorrido2.censistasNecesarios());
	}
	
	@Test
	public void testCaminoDeDosYtres() {
		_recorrido3.agregar(0);
		_recorrido3.agregar(1);
		_recorrido3.agregar(3);
		_recorrido3.agregar(2);
		_recorrido3.agregar(4);
		
		assertEquals(2, _recorrido3.censistasNecesarios());
	}
}
