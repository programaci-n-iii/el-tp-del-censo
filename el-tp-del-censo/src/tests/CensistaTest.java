package tests;

import static org.junit.Assert.assertEquals;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import main.Censista;

public class CensistaTest {
	
	private Censista _censista;

	@Before
	public void setUp() {
		_censista = new Censista(43928475, "Juan");
	}

	@Test
	public void asignacionDeManzanasTest() {
		_censista.asignar(0);
		_censista.asignar(1);
		_censista.asignar(2);
		
		List<Integer> esperado = List.of(0, 1, 2);
		assertManzanasDeCensista(esperado, _censista);
	}
	
	@Test
	public void asignacionDeManzanaRepetidaTest() {
		_censista.asignar(2);
		_censista.asignar(2);
		
		List<Integer> esperado = List.of(2);
		assertManzanasDeCensista(esperado, _censista);
	}
	
	@Test
	public void compararCensistasIgualesTest() {
		Censista censista2 = new Censista(43928475, "Juan");
		assertEquals(0, _censista.compareTo(censista2));
	}
	
	@Test
	public void compararCensistasDistintosTest() {
		Censista censista2 = new Censista(44928475, "Pedro");
		assertEquals(1, _censista.compareTo(censista2));
	}
	
	private void assertManzanasDeCensista(List<Integer> manzanas, Censista censista) {
		assertEquals(manzanas.size(), censista.cantidadManzanas());
		for(int i = 0; i < manzanas.size(); i++) {
			assertEquals(manzanas.get(i), Integer.valueOf(censista.getManzana(i)));
		}
	}

}
