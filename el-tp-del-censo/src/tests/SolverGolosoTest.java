package tests;

import static org.junit.Assert.assertEquals;
import java.util.Map;
import java.util.TreeMap;
import org.junit.Before;
import org.junit.Test;
import algoritmos.SolverGoloso;
import main.RadioCensal;

public class SolverGolosoTest {

	private RadioCensal _radio4Manzanas;
	private SolverGoloso _solver;

	@Before
	public void setUp() {
		_radio4Manzanas = new RadioCensal(4);
		_solver = new SolverGoloso(_radio4Manzanas);
		agregarCensistas(_radio4Manzanas);
	}

	@Test
	public void manzanasContiguasTest() {
		_radio4Manzanas.setManzanasVecinas(0, 1);
		_radio4Manzanas.setManzanasVecinas(1, 2);
		_radio4Manzanas.setManzanasVecinas(2, 3);

		_solver.resolver();
		
		assertEquals(2, _solver.getCantCensistasAsignados());
	}

	@Test
	public void tresManzanasContiguasTest() {
		RadioCensal radioTresManzanas = new RadioCensal(3);
		agregarCensistas(radioTresManzanas);
		radioTresManzanas.setManzanasVecinas(0, 1);
		radioTresManzanas.setManzanasVecinas(1, 2);

		_solver = new SolverGoloso(radioTresManzanas);
		_solver.resolver();

		assertEquals(1, _solver.getCantCensistasAsignados());
	}

	@Test
	public void sinManzanasContiguasTest() {
		_solver.resolver();

		assertEquals(4, _solver.getCantCensistasAsignados());
	}

	@Test
	public void dosComponentesConexasTest() {
		_radio4Manzanas.setManzanasVecinas(0, 1);
		_radio4Manzanas.setManzanasVecinas(2, 3);

		_solver.resolver();

		assertEquals(2, _solver.getCantCensistasAsignados());
	}

	@Test(expected = IllegalArgumentException.class)
	public void radioNullTest() {
		_solver = new SolverGoloso(null);
		_solver.resolver();
	}

	@Test(expected = IllegalArgumentException.class)
	public void sinCensistasTest() {
		RadioCensal radio = new RadioCensal(2);
		radio.setManzanasVecinas(0, 1);

		_solver = new SolverGoloso(radio);
		_solver.resolver();
	}

	
	@Test
	public void asignacionNoOptimaTest() {
		RadioCensal radio = new RadioCensal(5);
		agregarCensistas(radio);
		radio.setManzanasVecinas(0, 1);
		radio.setManzanasVecinas(1, 2);
		radio.setManzanasVecinas(2, 3);
		radio.setManzanasVecinas(2, 4);

		_solver = new SolverGoloso(radio);
		_solver.resolver();

		// solucion optima = 2
		assertEquals(3, _solver.getCantCensistasAsignados());
	}

	private void agregarCensistas(RadioCensal radio) {
		Map<Integer, String> censistas = new TreeMap<>();
		censistas.put(40758663, "A");
		censistas.put(38752684, "B");
		censistas.put(36283728, "C");
		censistas.put(39857263, "D");
		censistas.put(35472093, "E");
		for (Integer dni: censistas.keySet()) {
			radio.agregarCensista(dni, censistas.get(dni));
		}
	}

}