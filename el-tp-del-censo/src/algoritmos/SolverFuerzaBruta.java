package algoritmos;

import java.util.HashSet;
import java.util.Set;

import main.Censista;
import main.RadioCensal;

public class SolverFuerzaBruta {

	private Recorrido _actual, _mejorRecorrido;
	private final int _cantMaxManzanas = 3;
	private Integer _cantMinCensistas;
	private Set<Censista> _censistas;
	private Set<Censista> _censistasConManzanas;
	private boolean[] _manzanas;
	private int _nManzanas;
	private RadioCensal _radio;

	
	public SolverFuerzaBruta(RadioCensal r) {
		_radio = r;
		_nManzanas = r.tamano();
		_actual = new Recorrido(_radio);
		_cantMinCensistas = Integer.MAX_VALUE;
		_censistas = r.getCensistas();
		_manzanas = new boolean[r.tamano()];
		_censistasConManzanas = new HashSet<>();
		generarSolucion();
	}

	public Set<Censista> solucion() {
		return new HashSet<>(_censistasConManzanas);
	}
	
	public Recorrido getMejorRecorrido() {
		return new Recorrido(_mejorRecorrido);
	}

	private void generarSolucion() {
		boolean[] manzanas = new boolean[_nManzanas];
		crearRecorridos(manzanas);
		asignarCensistas();
	}

	private void crearRecorridos(boolean[] manzanas) {
		if (_actual.tamano() == _nManzanas && _actual.censistasNecesarios() < _cantMinCensistas) {
			_cantMinCensistas = _actual.censistasNecesarios();
			_mejorRecorrido = new Recorrido(_actual);
			return;
		}

		for (int i = 0; i < _nManzanas; i++) {
			if (!manzanas[i]) {
				_actual.agregar(i);
				manzanas[i] = true;
				crearRecorridos(manzanas);
				_actual.elminarUltimo();
				manzanas[i] = false;
			}
		}
	}

	/////////// Asignacion de censistas ///////////
	
	private void asignarCensistas() {
		for (Censista censista : _censistas) {
			for (Integer manzana : _mejorRecorrido.getManzanas()) {
				if (puedeAsignarse(censista, manzana)) {
					if (!_manzanas[manzana]) {
						asignarManzana(censista, manzana);
					} 
				} else {  
					break; // Para pasar directo al siguiente censista
				// Dado que se debe asignar segun el orden del mejor recorrido
				}
			}
		}
	}

	private void asignarManzana(Censista censista, Integer manzana) {
		censista.asignar(manzana);
		_manzanas[manzana] = true;
		if (!_censistasConManzanas.contains(censista)) {
			_censistasConManzanas.add(censista);
		}
	}
	
	private boolean puedeAsignarse(Censista censista, Integer manzana) {
		return !censista.tieneManzanas() || (censista.cantidadManzanas() < _cantMaxManzanas 
				&& existeManzanaVecina(censista, manzana));
	}
	
	private boolean existeManzanaVecina(Censista censista, Integer manzana) {
		boolean ret = false;
		
		for (Integer m : censista.getManzanas()) {
			ret = ret || (_radio.sonManzanasVecinas(m, manzana));
		}
		return ret;
	}
}
