package algoritmos;

import java.util.LinkedList;

import main.RadioCensal;

public class Recorrido {

	private LinkedList<Integer> _manzanas;
	private RadioCensal _radio;
	private int _cantMaxManzanas;

	public Recorrido(RadioCensal radio) {
		_radio = radio;
		_manzanas = new LinkedList<>();
		_cantMaxManzanas = 3;
	}

	public Recorrido(Recorrido r) {
		_radio = r.getRadio();
		_manzanas = r.getManzanas();
		_cantMaxManzanas = 3;
	}

	public int censistasNecesarios() {
		int ret = 1; // arrancan en uno considerando ya un primer censista
		int cont = 1; // para controlar condicion de no mas de tres manzanas
		int mActual, mSiguiente;
		// Recorre en el orden del recorrido
		for (int m = 0; m < _radio.tamano() - 1; m++) {
			mActual = _manzanas.get(m);
			mSiguiente = _manzanas.get(m + 1);

			if (_radio.sonManzanasVecinas(mActual, mSiguiente) && cont < _cantMaxManzanas) {
				cont++;
			}
			else {
				ret++;
				cont = 1; 
			}
		}
		return ret;
	}

	public void agregar(int manzana) {
		_manzanas.addLast(manzana);
	}

	public int tamano() {
		return _manzanas.size();
	}

	public boolean contiene(int manzana) {
		return _manzanas.contains(manzana);
	}

	public void elminarUltimo() {
		_manzanas.removeLast();
	}

	private RadioCensal getRadio() {
		return new RadioCensal(_radio);
	}

	public LinkedList<Integer> getManzanas() {
		LinkedList<Integer> ret = new LinkedList<>();

		for (Integer m : _manzanas) {
			ret.add(m);
		}
		return ret;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i = 0; i < _manzanas.size() - 1; i++) {
			sb.append(_manzanas.get(i));
			sb.append(", ");
		}
		sb.append(_manzanas.get(_manzanas.size() - 1));
		sb.append("]");
		return sb.toString();
	}
}
