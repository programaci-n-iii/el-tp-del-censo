package algoritmos;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import main.Censista;
import main.RadioCensal;

public class SolverGoloso {

	private RadioCensal _radio;
	private List<Integer> _manzanasVisitadas;
	private Set<Censista> _censistas;
	private int _cantCensistasAsignados;
	private final int _cantMaxManzanas = 3;
	
	public SolverGoloso(RadioCensal radio) {
		verificarRadio(radio);
		_radio = radio;
	}

	public void resolver() {
		verificarCantCensistas();
		incializarRecorrido();
		for (Censista censista : _censistas) {
			for (int manzana = 0; manzana < _radio.tamano(); manzana++) {
				if (!_manzanasVisitadas.contains(manzana)) {
					asignarManzana(manzana, censista);
				}
			}
		}
		_radio.setCensistas(_censistas);
	}

	private void asignarManzana(int manzana, Censista censista) {
		if (!censista.tieneManzanas()) {
			censista.asignar(manzana);
			_manzanasVisitadas.add(manzana);
			_cantCensistasAsignados++;
		} else {
			for(int i = 0; i < censista.cantidadManzanas(); i++) {
				int manzanaAsignada = censista.getManzana(i);
				if (puedeAsignarse(manzana, censista, manzanaAsignada)) {
					censista.asignar(manzana);
					_manzanasVisitadas.add(manzana);
				}
			}
		}
	}

	private boolean puedeAsignarse(int manzana, Censista censista, int manzanaAsignada) {
		return manzanaAsignada != manzana && _radio.sonManzanasVecinas(manzana, manzanaAsignada)
				&& censista.cantidadManzanas() < _cantMaxManzanas;
	}
	
	private void incializarRecorrido() {
		_manzanasVisitadas = new ArrayList<>();
		_cantCensistasAsignados = 0;
		_censistas = Censista.clonarCensistasSinManzanas(_radio.getCensistas());
	}

	private void verificarCantCensistas() {
		if (_radio.getCensistas().size() < _radio.tamano()) {
			throw new IllegalArgumentException("Censistas insuficientes: la cantidad de censistas debe ser mayor o igual a " + _radio.tamano()
							+ ". Cantidad de censistas: " + _radio.getCensistas().size());
		}
	}

	private void verificarRadio(RadioCensal radio) {
		if (radio == null) {
			throw new IllegalArgumentException("Radio invalido: El radio no debe ser nulo");
		}
	}

	public int getCantCensistasAsignados() {
		return _cantCensistasAsignados;
	}

}
